 ;;; lyx-formula.el --- Edit LaTeX formulas with the LyX GUI editor.  -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Leonardo Schwarz

;; Author: Leonardo Schwarz <mail@leoschwarz.com>
;; Keywords: convenience, tex
;; Version: 0.1.0
;; Dependencies: s auctex

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO (see README.org for now)

;;; Code:

(require 'filenotify)
(require 's)
(require 'texmathp)

(defconst lyx-formula-template-path
  (expand-file-name "template.lyx" (file-name-directory load-file-name)))

(defcustom lyx-formula-indent t "true if inserted formulas should be indented")

;; TODO configurable?
(defvar lyx-formula-binary-path (locate-file "lyx" exec-path exec-suffixes 'executable))

(defcustom lyx-formula-pipe-path
  (expand-file-name "~/.lyx/lyxpipe")
  "path to lyxpipe (without .in/.out suffix)")

(cl-defstruct (lyx-formula-handle (:constructor lyx-formula-handle-create)
				  (:copier nil))
  region-begin
  region-end
  file-handle)

;; wait until file exists or timeout occurs
(defun lyx-formula-wait-for-file (file timeout-sec)
  (let ((started-at (current-time))
	(success nil))
    (while
	(and (not success)
	     (<= (float-time (time-subtract (current-time) started-at)) timeout-sec))
      (if (file-exists-p file) (setq success t) (sleep-for 0.1)))
    (if (not success) (message (concat "timed out waiting for " file)))
    success
    ))

;; TODO do we need the hello and bye messages?
;; TODO check success?
(defun lyx-formula-open-file (filename)
  (let ((server-pipe (concat lyx-formula-pipe-path ".in"))
	(message (concat
		  "LYXSRV:emacslyx:hello\n"
		  (concat "LYXCMD:emacslyx:file-open:" filename "\n")
		  "LYXSRV:emacslyx:bye\n"
		  )))
    (if (file-exists-p server-pipe)
	(write-region message nil server-pipe t 'quiet)
      ;; TODO use something more robust than pgrep (and cross platform)
      ;; pgrep status 1 -> not running, 0 -> running, 127 -> pgrep not installed
      (let ((status-code (shell-command "pgrep lyx")))
	(if (eq status-code 1)
	    ;; Launch lyx and see if it helps.
	    (progn
	      (message "launching lyx")
	      (call-process lyx-formula-binary-path nil 0)
	      (if (lyx-formula-wait-for-file server-pipe 10.0)
		  (lyx-formula-open-file filename)))
	  (if (eq status-code 0)
	      (error "lyxpipe not configured properly")
	    (error "unknown error (pgrep not installed on system?)"))
	  ))
      )
    )
  )

(defun lyx-formula-insert-indented (text)
  (let* ((margin (- (point) (line-beginning-position)))
	 (prefix (make-string margin ? )))
    (insert (replace-regexp-in-string "\n" (concat "\n" prefix) text))))

(defun lyx-formula-handle-buffer (handle)
  (marker-buffer (lyx-formula-handle-region-begin handle)))

(defun lyx-formula-file-was-updated (event)
  "return filename if target file was updated by lyx, otherwise nil"
  (let ((eventtype (nth 1 event)))
    (cond ((string= eventtype "created")
	   (nth 2 event))
	  ((string= eventtype "renamed")
	   ;; lyx does two kinds of rename
	   ;; - "myfile.lyx" -> "oldfile.lyx~"
	   ;; - "tempflile.lyx" -> "myfile.lyx" (we're interested in this one)
	   (if (s-suffix? ".lyx" (nth 3 event))
	       (nth 3 event)
	     nil))
	  (t nil))))

(defun lyx-formula-update-from-tempfile (event)
  (let ((filename (lyx-formula-file-was-updated event))
	(reg-begin (marker-position (lyx-formula-handle-region-begin lyx-temp)))
	(reg-end (marker-position (lyx-formula-handle-region-end lyx-temp)))
	(buffer (lyx-formula-handle-buffer lyx-temp))
	newformula
	src-begin
	src-end)
    (if filename
	(progn
	  ;; Extract the relevant part of lyx file.
	  (setq newformula
		(with-temp-buffer
		  (insert-file-contents filename)
		  (search-forward "BEGIN EDITAREA")
		  (setq src-begin
			(- (search-forward "$") 1))
		  (search-forward "END EDITAREA")
		  (setq src-end
			(+ (search-backward "$") 1))
		  (buffer-substring src-begin src-end)))
	  ;;(message "new formula: %s" newformula)
	  ;;(message "old region: %i %i" reg-begin reg-end)
	  ;; Perform replace.
	  (with-current-buffer buffer
	    (delete-region reg-begin reg-end)
	    (goto-char reg-begin)
	    (if lyx-formula-indent
		(lyx-formula-insert-indented newformula)
	      (insert newformula)))
	  ))))

(defun lyx-formula-cancel-all ()
  "Cancel current open formula action."
  (interactive)
  (if (boundp 'lyx-temp)
      (progn
	(message "cancelling lyx edit")
	(let (file-handle (lyx-formula-handle-file-handle lyx-temp))
	  (if file-handle (file-notify-rm-watch file-handle)))
	(set-marker (lyx-formula-handle-region-begin lyx-temp) nil)
	(set-marker (lyx-formula-handle-region-end lyx-temp) nil)
	(makunbound 'lyx-temp)
	)
    )
  )

;; All would be easiest if regex (?<!\\)\$)) could be evaluated directly with
;; search-backward-regexp and search-forward-regexp but it doesn't support lookahead... :/
(defun lyx-formula-find-end ()
  (loop
   (search-forward "$")
   (if (not (string=
	     (buffer-substring (- (point) 2) (- (point) 1))
	     "\\"))
       (return (point)))))
(defun lyx-formula-find-begin ()
  (loop
   (search-backward "$")
   (if (not (string=
	     (buffer-substring (- (point) 1) (point))
	     "\\"))
       (return (point)))))

;; Not used currently, work in progress formula detection using texmathp.
;; TODO: This is very inefficient. It's more correct with regards to nested subformulas,
;;       but I'm not sure if it's worth the computationl effort, for very long formulas
;;       it can take a couple seconds.
(defun lyx-formula-find ()
  (if (texmathp)
      (let* ((formula-outer-begin (cdr texmathp-why))
	     (formula-prefix (car texmathp-why))
	     (formula-inner-begin (+ formula-outer-begin (length formula-prefix)))
	     formula-outer-end formula-inner-end)
	(goto-char formula-inner-begin)
	(while (texmathp) (forward-char 1))
	;;(while (and (texmathp) (< (point) (point-max))) (forward-char 1))
	;; TODO check if this fails in corner case where last char closes formula
	;;(if (texmathp) (error "formula not closed at EOF"))
	;; Return the obtained information
	(setq formula-outer-end (- (point) 1))

	;; TODO this fails for \begin{}...\end{} formulas
	(setq formula-inner-end (- formula-outer-end (length formula-prefix)))

	(list formula-outer-begin formula-outer-end
	      formula-inner-begin formula-inner-end)
	)
    nil))

(defun lyx-formula-edit ()
  "Edit formula at point."
  (interactive)
  (let*
      ;; Determine the selected text
      ((formula-begin (save-excursion (lyx-formula-find-begin)))
       (formula-end (save-excursion (lyx-formula-find-end)))
       (text-old (buffer-substring formula-begin formula-end))
       (template-text
	(with-temp-buffer
	  (insert-file-contents lyx-formula-template-path)
	  (search-forward "FORMULA_PLACEHOLDER")
	  (replace-match text-old t t)
	  (buffer-string)))
       (tempfile
	(make-temp-file "emacslyx" nil ".lyx")))

    ;;(message "text old : %s" text-old)

    ;; Write template text to file.
    (write-region template-text nil tempfile)

    ;; TODO
    (lyx-formula-cancel-all)

    (setq lyx-temp (lyx-formula-handle-create
		    :region-begin (copy-marker formula-begin nil)
		    :region-end (copy-marker formula-end t)
		    :file-handle nil))

    ;; Update the formula once the file is saved.
    (setf (lyx-formula-handle-file-handle lyx-temp)
	  (file-notify-add-watch
	   tempfile
	   '(change)
	   'lyx-formula-update-from-tempfile))

    ;; Open the file in lyx. (TODO check return code)
    (lyx-formula-open-file tempfile)

    )
  )

(defun lyx-formula-insert ()
  "Insert a new formula at point."
  (interactive)
  (progn
    (insert "$ $")
    (goto-char (- (point) 1))
    (lyx-formula-edit)))

(provide 'lyx-formula)

;;; lyx-formula.el ends here
